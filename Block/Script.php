<?php
namespace Pushassist\Webpushnotification\Block;

use Magento\Framework\View\Element\Context;

class Script extends \Magento\Framework\View\Element\Template 
{ 
     
    protected $_config;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
	
	parent::__construct($context, $data);
      
    }

    public function getJsrestrict(){
	return $manual_js=$this->scopeConfig->getValue('pushassistsection/general/pushassist_js_restrict',ScopeInterface::SCOPE_STORE);
    }

    public function getJspath(){
       return $js_path=$this->scopeConfig->getValue('pushassistsection/general/jsPath',ScopeInterface::SCOPE_STORE);
    }

}
