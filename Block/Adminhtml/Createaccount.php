<?php
namespace Pushassist\Webpushnotification\Block\Adminhtml;

use Magento\Backend\Block\Template;

class Createaccount extends Template {
   
    protected $_helper;

    /**
    * @param Context $context
    * @param array $data
    */
    public function __construct(
        Template\Context $context,
	\Pushassist\Webpushnotification\Helper\Data $datahelper,
        array $data = []
    ) {
	$this->_helper = $datahelper;
        parent::__construct($context, $data);
    }

    public function getcheckAccount(){ 
	return $this->_helper->get_account_details();
    }
    public function getApiKey(){
	return $this->_helper->getApiKey();
    }
    public function getSecretKey(){
	  return $this->_helper->getSecretKey();
	
    }
 }
