<?php
namespace Pushassist\Webpushnotification\Block\Adminhtml;

use Magento\Backend\Block\Template;

class Notificationsend extends Template {
   
    protected $_helper;

    /**
    * @param Context $context
    * @param array $data
    */
    public function __construct(
        Template\Context $context,
	\Pushassist\Webpushnotification\Helper\Data $datahelper,
        array $data = []
    ) {
	$this->_helper = $datahelper;
        parent::__construct($context, $data);
      
     }

    public function getcheckAccount(){
	return $this->_helper->get_account_details();
    }
    public function get_dashboardData(){
	return $this->_helper->get_dashboard();
    }
    public function get_segmentsData(){
       return $this->_helper->get_segments();
    }
    
 }
