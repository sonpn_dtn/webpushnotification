<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Notificationsend;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Add extends Action
{
    protected $_resource;
    protected $_filesystem;
    protected $_directory;
    protected $_fileUploaderFactory;
    
    public function __construct(
	Context $context,
	\Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
	\Magento\Framework\Filesystem $filesystem,
	\Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource
    ) {
	$this->_fileUploaderFactory = $fileUploaderFactory;
	$this->_filesystem = $filesystem;
	$this->_directory = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
	$this->_resource = $resource;
	parent::__construct($context);
    }

    public function execute() { 
	$resultRedirect = $this->resultRedirectFactory->create();
	$post=$this->getRequest()->getPost();

	if($post){

	      $full_image_path='';

	      try {
		/** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
		$uploader = $this->_fileUploaderFactory->create(['fileId' => 'fileupload']);
		$uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
		$uploader->setAllowRenameFiles(true);
		$uploader->setFilesDispersion(true);
		$path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('pushassist/');
		$uploader->save($path);
		$fileName = $uploader->getUploadedFileName();
		$full_image_path=$this->storeManager->getStore()->getBaseUrl().'pub/media/pushassist'.$fileName;
	      } catch (\Exception $e) {
		
	      }

	      $response_array = array("notification" => array(
						"title" => $post['title'],
						"message" => $post['message'],
						"redirect_url" => $post['url'],
						"image" => $full_image_path)
	      );
    
	      if(isset($post['is_utm_show'])){

		if($post['is_utm_show']==1){

		 $response_array['utm_params'] =array("utm_source" => $post['utm_source'],	// optional
							"utm_medium" => $post['utm_medium'],
							"utm_campaign" => $post['utm_campaign']);
		}
	      }

	      if(!empty($post['segment'])){
		  $response_array['segments'] =$post['segment'];
	      }
	   
	      $result_array =$this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->send_notification($response_array);
	  
	      if($result_array['status'] == 'Success'){
		      $this->messageManager->addSuccess(__($result_array['response_message']));
		       return $resultRedirect->setPath('pushassist/notificationsend/index/',['_current' => true]);
	      }else if($result_array['status'] == 'Error') {
		      $this->messageManager->addError(__($result_array['error_message']));
		       return $resultRedirect->setPath('pushassist/notificationsend/index/',['_current' => true]);
		      
	      }else if($result_array['error'] != '') {
		      $this->messageManager->addError(__($result_array['error']));
		      $message = __($result_array['error']);
		       return $resultRedirect->setPath('pushassist/notificationsend/index/',['_current' => true]);
		    
	      }else {
		      $this->messageManager->addError(__($result_array['errors']));
		      return $resultRedirect->setPath('pushassist/notificationsend/index/',['_current' => true]);
	      }
	 }
    }
     
    
}

