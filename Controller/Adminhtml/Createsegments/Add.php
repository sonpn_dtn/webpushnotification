<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Createsegments;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;

class Add extends Action {
    
    protected $_resource;
   
    public function __construct(
	Context $context,
	\Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource
    ) { 
	$this->_resource = $resource;
	parent::__construct($context);
    }

    public function execute() { 
      $resultRedirect = $this->resultRedirectFactory->create();
      $post=$this->getRequest()->getPost();

	  if($post){

	      $response_array = array("segment" => array( "segment_name" => urlencode($post['title'])));
	      $result_array =$this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->add_segments($response_array);
	  
	      if($result_array['status'] == 'Success'){
		      $this->messageManager->addSuccess(__($result_array['response_message']));
		      return $resultRedirect->setPath('pushassist/segments/index/',['_current' => true]);
	      }else if($result_array['status'] == 'Error') {
		      $this->messageManager->addError(__($result_array['error_message']));
		      return $resultRedirect->setPath('pushassist/createsegments/index/',['_current' => true]);
	      }else if($result_array['error'] != '') {
		      $this->messageManager->addError(__($result_array['error']));
		      return $resultRedirect->setPath('pushassist/createsegments/index/',['_current' => true]);
	      }else {
		      $this->messageManager->addError(__($result_array['errors']));
		      return $resultRedirect->setPath('pushassist/createsegments/index/',['_current' => true]);
	      }
	}
     
    }
}

