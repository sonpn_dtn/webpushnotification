<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Createcampaign;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;

class Add extends Action {
    
    protected $_resource;
    
    public function __construct(
	Context $context,
	\Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource
    ) {
	$this->_scopeConfig = $scopeConfig;
	$this->_resource = $resource;
	parent::__construct($context);
    }

    public function execute() { 
      $resultRedirect = $this->resultRedirectFactory->create();
      $post=$this->getRequest()->getPost();

	  if($post){

	      $full_image_path='';

	      try {
		/** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
		$uploader = $this->_fileUploaderFactory->create(['fileId' => 'fileupload']);
		$uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
		$uploader->setAllowRenameFiles(true);
		$uploader->setFilesDispersion(true);
		$path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('pushassist/');
		$uploader->save($path);
		$fileName = $uploader->getUploadedFileName();
		$full_image_path=$this->_storeManager->getStore()->getBaseUrl().'pub/media/pushassist'.$fileName;
	      } catch (\Exception $e) {
		
	      }

	      $response_array = array("campaign" => array(
						"title" => $post['title'],
						"message" => $post['message'],
						"redirect_url" => $post['url'],
						"timezone"=>$post['campaigndate'],
						"image" => $full_image_path)
	      );
	      if(isset($post['is_utm_show'])){
		if($post['is_utm_show']==1){

		 $response_array['utm_params'] =array("utm_source" => $post['utm_source'],	// optional
							"utm_medium" => $post['utm_medium'],
							"utm_campaign" => $post['utm_campaign']);
 
		}
	     }

	     if(!empty($post['segment'])){
		  $response_array['segments'] =$post['segment'];
	     }

	    
	     $result_array =$this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->add_campaigns($response_array);
	  
	      if($result_array['status'] == 'Success'){

		      $this->messageManager->addSuccess(__($result_array['response_message']));
		      return $resultRedirect->setPath('pushassist/campaign/index/',['_current' => true]);
	      }else if($result_array['status'] == 'Error') {
		      
		      $this->messageManager->addError(__($result_array['error_message']));
		      return $resultRedirect->setPath('pushassist/campaign/index/',['_current' => true]);
		      
	      }else if($result_array['error'] != '') {
		      
		      $this->messageManager->addError(__($result_array['error']));
		      return $resultRedirect->setPath('pushassist/campaign/index/',['_current' => true]);
		    
	      }else {
		      $this->messageManager->addError(__($result_array['errors']));
		      return $resultRedirect->setPath('pushassist/campaign/index/',['_current' => true]);
	      }
	 }
     
    }
}

