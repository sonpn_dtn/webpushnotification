<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Createaccount;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;;

class Login extends Action {
    
    protected $_resource;
    
    public function __construct(
	Context $context,
	\Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource
    ) {
	$this->_resource = $resource;
	parent::__construct($context);
    }

    public function execute() { 
      $resultRedirect = $this->resultRedirectFactory->create();
      $post=$this->getRequest()->getPost();
      $result_array =$this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->check_account_details($post);
      
      if(isset($result_array['apiKey'])){

		    if($result_array['apiKey'] != ''){

			$apiKey=$result_array['apiKey'];
			$apiSecret=$result_array['apiSecret'];
			$planType=$result_array['planType'];
			$subscribers_limit=$result_array['subscribers_limit'];
			$subscribers_remain=$result_array['subscribers_remain'];
			$jsPath=$result_array['jsPath'];

			if( $apiKey != '' ){
			      $this->_resource->saveConfig('pushassistsection/general/apikey', $apiKey, 'default', 0);
			}
			if($apiSecret != ''){
			      $this->_resource->saveConfig('pushassistsection/general/secretkey', $apiSecret, 'default', 0);
			}
			if($subscribers_remain != ''){
			      $this->_resource->saveConfig('pushassistsection/general/subscribers_remain', $subscribers_remain, 'default', 0);
			}
			if( $planType != '' ){
			      $this->_resource->saveConfig('pushassistsection/general/planType', $planType, 'default', 0);
			}
			if($subscribers_limit != ''){
			      $this->_resource->saveConfig('pushassistsection/general/subscribers_limit', $subscribers_limit, 'default', 0);
			}
			if($subscribers_remain != ''){
			      $this->_resource->saveConfig('pushassistsection/general/subscribers_remain', $subscribers_remain, 'default', 0);
			}
			if($jsPath != ''){
			      $this->_resource->saveConfig('pushassistsection/general/jsPath', $jsPath, 'default', 0);
			}
			if(isset($result_array['response_message'])){
			$message = __($result_array['response_message']);}else{$message =''; }
			$this->messageManager->addSuccess(__($message));
			return $resultRedirect->setPath('pushassist/notificationdashboard/index/',['_current' => true]);
			
		    }
		}

		if(isset($result_array['status'])){
		    if($result_array['status'] == 'Error') {
			  $this->messageManager->addError(__($result_array['error_message']));
			  return $resultRedirect->setPath('pushassist/createaccount/index/',['_current' => true]);
		    }
		}
		
		if(isset($result_array['error'])){
		    if($result_array['error'] != '') {
			  $this->messageManager->addError(__($result_array['error']));
			  return $resultRedirect->setPath('pushassist/createaccount/index/',['_current' => true]);
		    } 
		}
	}
}

