<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Createaccount;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;

class Register extends Action {
    
    protected $_resource;
    
    public function __construct(
	Context $context,
	\Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource
    ) {
	$this->_resource = $resource;
	parent::__construct($context);
    }

    public function execute() { 
      $resultRedirect = $this->resultRedirectFactory->create();
      $post=$this->getRequest()->getPost();

      if($post){
	  
		$response_array = array("account" => array(
							"name" => trim($post['name']),
							"company_name" => trim($post['company_name']),
							"contact" => trim($post['phone']),
							"email" => trim($post['email']),
							"password" => trim($post['password']),
							"protocol" => trim($post['protocol']),
							"siteurl" => trim($post['site_url']),
							"subdomain" => trim($post['sub_domain']),
							"psa_source" => 'Magento 2'
						    )
		);

		$result_array =$this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->create_account($response_array);

		if($result_array['status'] == 'Success'){
		  
		      $account_info=array();
		      $account_info['api_key'] =$result_array['api_key'];
		      $account_info['secret_key'] =$result_array['auth_secret'];

		      $get_account_info = $this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->check_account_details($account_info);
    
		      $apiKey=$get_account_info['apiKey'];
		      $apiSecret=$get_account_info['apiSecret'];
		      $planType=$get_account_info['planType'];
		      $subscribers_limit=$get_account_info['subscribers_limit'];
		      $subscribers_remain=$get_account_info['subscribers_remain'];
		      $jsPath=$get_account_info['jsPath'];

		      if( $apiKey != '' ){
			    $this->_resource->saveConfig('pushassistsection/general/apikey', $apiKey, 'default', 0);
		      }
		      if($apiSecret != ''){
			    $this->_resource->saveConfig('pushassistsection/general/secretkey', $apiSecret, 'default', 0);
		      }
		      if($subscribers_remain != ''){
			    $this->_resource->saveConfig('pushassistsection/general/subscribers_remain', $subscribers_remain, 'default', 0);
		      }
		      if( $planType != '' ){
			    $this->_resource->saveConfig('pushassistsection/general/planType', $planType, 'default', 0);
		      }
		      if($subscribers_limit != ''){
			    $this->_resource->saveConfig('pushassistsection/general/subscribers_limit', $subscribers_limit, 'default', 0);
		      }
		      if($subscribers_remain != ''){
			    $this->_resource->saveConfig('pushassistsection/general/subscribers_remain', $subscribers_remain, 'default', 0);
		      }
		      if($jsPath != ''){
			    $this->_resource->saveConfig('pushassistsection/general/jsPath', $jsPath, 'default', 0);
		      }

		      $this->messageManager->addSuccess(__($result_array['response_message']));
		      return $resultRedirect->setPath('pushassist/notificationdashboard/index/',['_current' => true]);
		}elseif($result_array['status'] == 'Error') { 
		      $this->messageManager->addError(__($result_array['error_message']));
		      return $resultRedirect->setPath('pushassist/createaccount/index/',['_current' => true]);
		}elseif($result_array['error'] != '') {
		      $this->messageManager->addError(__($result_array['error']));
		      return $resultRedirect->setPath('pushassist/createaccount/index/',['_current' => true]);
		}else {
		      $this->messageManager->addError(__($result_array['errors']));
		      return $resultRedirect->setPath('pushassist/createaccount/index/',['_current' => true]);
		}
	}

    }
}

