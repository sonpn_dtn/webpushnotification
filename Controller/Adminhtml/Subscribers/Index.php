<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Subscribers;

class Index extends \Magento\Backend\App\Action
{
  public function __construct(
	  \Magento\Backend\App\Action\Context $context
    ) {
	  parent::__construct($context);
    }

    public function execute(){   

      $resultRedirect = $this->resultRedirectFactory->create();
      $apiKey = $this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->getApiKey();
      $secretKey =$this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->getSecretKey();

      if($apiKey !='' && $secretKey !=''){
		$this->_view->loadLayout();
		$this->_view->renderLayout();
      }else{
	       return $resultRedirect->setPath('pushassist/createaccount/index/',['_current' => true]);
      }
     
   }
}
 
