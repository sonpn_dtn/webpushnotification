<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Settings;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Gsmsetting extends Action {
   
    protected $_resource;
    
    public function __construct(
	Context $context,
	\Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource
    ) { 
	$this->_resource = $resource;
	parent::__construct($context);
    }


    public function execute() {
      $resultRedirect = $this->resultRedirectFactory->create();
      $post=$this->getRequest()->getPost();

	if($post){

	   $response_array = array("accountgcmsetting" => array("project_number" => $post['pushassist_gcm_project_no'],
							        "project_api_key" => trim($post['pushassist_gcm_api_key'])));
					
	   $result_array=$this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->gcm_setting($response_array);  
	   

		if($result_array['status'] == 'Success'){
		      $this->messageManager->addSuccess(__($result_array['response_message']));
		      return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
		} else if($result_array['status'] == 'Error') {
		      
		      $this->messageManager->addError(__($result_array['error_message']));
		      return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
		} else if($result_array['error'] != '') {
		      
		      $this->messageManager->addError(__($result_array['error']));
		      return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
		}
		else {
		     $this->messageManager->addError(__($result_array['errors']));
		     return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
		}
	}
      
    }
}

