<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Settings;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;

class Autosendproduct extends Action
{
  
    protected $_resource;

    public function __construct(
	Context $context,
	\Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource
    ) {
	$this->_resource = $resource;
	parent::__construct($context);
    }

    public function execute() { 
	  $resultRedirect = $this->resultRedirectFactory->create();
	  $post=$this->getRequest()->getPost();
	  if($post){
	      if(isset($post['pushassist_js_restrict'])){
		      $this->_resource->saveConfig('pushassistsection/general/pushassist_js_restrict', $post['pushassist_js_restrict'], 'default',  0);
	      }else{
		      $this->_resource->saveConfig('pushassistsection/general/pushassist_js_restrict', $post['pushassist_js_restrict'], 'default' ,0);
	      }
	      $this->_resource->saveConfig('pushassistsection/general/pushassist_setting_post_message', $post['pushassist_setting_post_message'], 'default' ,0);
	     
	  }
	 $this->messageManager->addSuccess(__('Update Successfully'));
	 return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
	
    }
}

