<?php
namespace Pushassist\Webpushnotification\Controller\Adminhtml\Settings;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Setting extends Action {
   
    protected $_resource;
    protected $_filesystem;
    protected $_directory;
    protected $_fileUploaderFactory;
   
    public function __construct(
	Context $context,
	\Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
	\Magento\Framework\Filesystem $filesystem,
	\Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource
    ) {
	$this->_fileUploaderFactory = $fileUploaderFactory;
	$this->_filesystem = $filesystem;
	$this->_directory = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
	$this->_resource = $resource;
	parent::__construct($context);
    }


    public function execute() { 
      $resultRedirect = $this->resultRedirectFactory->create();
      $post=$this->getRequest()->getPost();
      
      if($post){

	      $new_file_name='';
	      $full_image_path='';

	      try {
		/** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
		$uploader = $this->_fileUploaderFactory->create(['fileId' => 'fileupload']);
		$uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
		$uploader->setAllowRenameFiles(true);
		$uploader->setFilesDispersion(true);
		$path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('pushassist/site/');
		$uploader->save($path);
		$fileName = $uploader->getUploadedFileName();
		$get_file_name=explode("/",$fileName);
		$new_file_name=$get_file_name[3];
		$full_image_path=$this->storeManager->getStore()->getBaseUrl().'pub/media/pushassist/site'.$fileName;
	      } catch (\Exception $e) {
		
	      }

		$response_array = array("templatesetting" => array("interval_time" => $post['pushassist_timeinterval'],
							"opt_in_title" => trim($post['pushassist_opt_in_title']),
							"opt_in_subtitle" => trim($post['pushassist_opt_in_subtitle']),
							"allow_button_text" => trim($post['pushassist_allow_button_text']),
							"disallow_button_text" => trim($post['pushassist_disallow_button_text']),
							"template" => $post['template'],
							"location" => $post['psa_template_location'],
							"image_data" => $full_image_path,	// read image file & pass image data
							"image_name" => trim($new_file_name),
							"child_window_text" => trim($post['pushassist_child_window_text']),
							"child_window_title" => trim($post['pushassist_child_window_title']),
							"child_window_message" => trim($post['pushassist_child_window_message']),
							"notification_title" => trim($post['pushassist_setting_title']),
							"notification_message" => trim($post['pushassist_setting_message']),
							"redirect_url" => trim($post['pushassist_redirect_url']))
						);
	      
		  
		  
		  $result_array = $this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->settings($response_array);

		    if($result_array['status'] == 'Success'){
			  $this->messageManager->addSuccess(__($result_array['response_message']));
			  return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
			
		    }elseif($result_array['status'] == 'Error') {
			  $this->messageManager->addError(__($result_array['error_message']));
			 return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
		    }elseif($result_array['error'] != '') {
			  $this->messageManager->addError(__($result_array['error']));
			 return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
		    }else {
			  $this->messageManager->addError(__($result_array['errors']));
			  return $resultRedirect->setPath('pushassist/settings/index/',['_current' => true]);
		    }
	    }
     
    }
}

