<?php
namespace Pushassist\Webpushnotification\Helper;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_httpClientFactory;

    public function __construct(
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
	\Magento\Framework\App\Helper\Context $context
	 
    ) {
	$this->_httpClientFactory = $httpClientFactory;
	parent::__construct($context);
    }

    
    public function create_account($create_account_data) {
	  try {
            $create_account_request_json = json_encode($create_account_data);
            $client_create_account = $this->_httpClientFactory->create();
            $client_create_account->setUri('https://api.pushassist.com/accounts/');

            $client_create_account->setHeaders('Content-Type', 'application/json');
            $client_create_account->setMethod(\Zend_Http_Client::POST);
            $client_create_account->setRawData($create_account_request_json);
            $response_create_account = $client_create_account->request()->getBody();
	  } catch (\Exception $e) {
                $response_create_account = "";
            }
            
            return json_decode($response_create_account, true);
    }

    public function check_account_details($login_account_data){
	
	try {
                $check_api_key = $login_account_data['api_key'];
		$check_secret_key = $login_account_data['secret_key'];
		
                $client_account_details = $this->_httpClientFactory->create();
                $client_account_details->setUri('https://api.pushassist.com/accounts_info/');
		$client_account_details->setHeaders('X-Auth-Token', $check_api_key);
		$client_account_details->setHeaders('X-Auth-Secret', $check_secret_key);
		$client_account_details->setHeaders('Content-Type', 'application/json');
		$client_account_details->setMethod(\Zend_Http_Client::GET);
		$account_details_result = $client_account_details->request()->getBody();

            } catch (\Exception $e) {
               $account_details_result = "";
            }
	    return json_decode($account_details_result, true);
    }

    public function get_account_details() {
	   try {
		   
		$check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
		$check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);

		$client_account_details = $this->_httpClientFactory->create();
                $client_account_details->setUri('https://api.pushassist.com/accounts_info/');
		$client_account_details->setHeaders('X-Auth-Token', $check_api_key);
		$client_account_details->setHeaders('X-Auth-Secret', $check_secret_key);
		$client_account_details->setHeaders('Content-Type', 'application/json');
		$client_account_details->setMethod(\Zend_Http_Client::GET);
		$account_details_result = $client_account_details->request()->getBody();

	    } catch (\Exception $e) {
                $account_details_result = "";
            }
	    return json_decode($account_details_result, true);
     }

     public function get_dashboard(){
            try {
		$check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
		$check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);

		$client_dashboard = $this->_httpClientFactory->create();
                $client_dashboard->setUri('https://api.pushassist.com/dashboard/');
		$client_dashboard->setHeaders('X-Auth-Token', $check_api_key);
		$client_dashboard->setHeaders('X-Auth-Secret', $check_secret_key);
		$client_dashboard->setHeaders('Content-Type', 'application/json');
		$client_dashboard->setMethod(\Zend_Http_Client::GET);
		$dashboard_result = $client_dashboard->request()->getBody();


            } catch (\Exception $e) {
                $dashboard_result = "";
            }
	    return json_decode($dashboard_result, true);
     }

     public function send_notification($response_array) {


            try {
                $check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
		$check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);

                $send_notification_request_json = json_encode($response_array);

                $client_send_notification = $this->_httpClientFactory->create();
                $client_send_notification->setUri('https://api.pushassist.com/notifications/');
                $client_send_notification->setHeaders('X-Auth-Token', $check_api_key);
                $client_send_notification->setHeaders('X-Auth-Secret', $check_secret_key);
                $client_send_notification->setHeaders('Content-Type', 'application/json');
                $client_send_notification->setMethod(\Zend_Http_Client::POST);
                $client_send_notification->setRawData($send_notification_request_json);
                $response_send_notification = $client_send_notification->request()->getBody();
            } catch (\Exception $e) {
                $response_send_notification = "";
            }
	      return json_decode($response_send_notification, true);

    }

    public function add_segments($segment_response_array) {
	    try{
		$check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
		$check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
		$segment_request_json = json_encode($segment_response_array);

		$client_add_segments = $this->_httpClientFactory->create();
		$client_add_segments->setUri('https://api.pushassist.com/segments/');
		$client_add_segments->setHeaders('X-Auth-Token', $check_api_key);
		$client_add_segments->setHeaders('X-Auth-Secret', $check_secret_key);
		$client_add_segments->setHeaders('Content-Type', 'application/json');
		$client_add_segments->setMethod(\Zend_Http_Client::POST);
		$client_add_segments->setRawData($segment_request_json);
		$response_add_segments = $client_add_segments->request()->getBody();
	    } catch (\Exception $e) {
                $response_add_segments = "";
            }
	    return json_decode($response_add_segments, true);
    }

    public function get_segments() {
	    try{
		$check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
		$check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
		$client_segments = $this->_httpClientFactory->create();
		$client_segments->setUri('https://api.pushassist.com/segments/');
		$client_segments->setHeaders('X-Auth-Token', $check_api_key);
		$client_segments->setHeaders('X-Auth-Secret', $check_secret_key);
		$client_segments->setHeaders('Content-Type', 'application/json');
		$client_segments->setMethod(\Zend_Http_Client::GET);
		$segments_result = $client_segments->request()->getBody();
	    } catch (\Exception $e) {
                $segments_result = "";
            }
            return json_decode($segments_result, true);
    }

    public function get_subscribers() {
	  try{
            $check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
	    $check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
	    $client_subscribers = $this->_httpClientFactory->create();
	    $client_subscribers->setUri('https://api.pushassist.com/subscribers/');
	    $client_subscribers->setHeaders('X-Auth-Token', $check_api_key);
            $client_subscribers->setHeaders('X-Auth-Secret', $check_secret_key);
            $client_subscribers->setHeaders('Content-Type', 'application/json');
            $client_subscribers->setMethod(\Zend_Http_Client::GET);
            $subscribers_result = $client_subscribers->request()->getBody();
	  } catch (\Exception $e) {
                $subscribers_result = "";
            }
            return json_decode($subscribers_result, true);
    }

    public function get_notifications() {
	  try{
            $check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
	    $check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
	  
	    $client_notifications = $this->_httpClientFactory->create();
	    $client_notifications->setUri('https://api.pushassist.com/notifications/?per_page=10');
	    $client_notifications->setHeaders('X-Auth-Token', $check_api_key);
            $client_notifications->setHeaders('X-Auth-Secret', $check_secret_key);
            $client_notifications->setHeaders('Content-Type', 'application/json');
            $client_notifications->setMethod(\Zend_Http_Client::GET);
            $notifications_result = $client_notifications->request()->getBody();
	  } catch (\Exception $e) {
                $notifications_result = "";
	  }
            return json_decode($notifications_result, true);
    }

    public function get_notifications_by_count($counts) {
	  try{
            $check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
	    $check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);

	    $client_notifications_by_count = $this->_httpClientFactory->create();
	    $client_notifications_by_count->setUri('https://api.pushassist.com/notifications/?per_page='.$counts);
	    $client_notifications_by_count->setHeaders('X-Auth-Token', $check_api_key);
            $client_notifications_by_count->setHeaders('X-Auth-Secret', $check_secret_key);
            $client_notifications_by_count->setHeaders('Content-Type', 'application/json');
            $client_notifications_by_count->setMethod(\Zend_Http_Client::GET);
            $notifications_by_count_result = $client_notifications_by_count->request()->getBody();
	  } catch (\Exception $e) {
                $notifications_by_count_result = "";
	  }
            return json_decode($notifications_by_count_result, true);
    }
    
    public function get_campaigns(){
	   try{
            $check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
	    $check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);

	    $client_campaigns = $this->_httpClientFactory->create();
	    $client_campaigns->setUri('https://api.pushassist.com/campaigns');
            $client_campaigns->setHeaders('X-Auth-Token', $check_api_key);
            $client_campaigns->setHeaders('X-Auth-Secret', $check_secret_key);
            $client_campaigns->setHeaders('Content-Type', 'application/json');
            $client_campaigns->setMethod(\Zend_Http_Client::GET);
            $campaigns_result = $client_campaigns->request()->getBody();
	  } catch (\Exception $e) {
                $campaigns_result = "";
	  }
            return json_decode($campaigns_result, true);
    }
    
    public function add_campaigns($campaigns_response_array){
	  try{
	    $check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
	    $check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
	    $add_campaigns_request_json = json_encode($campaigns_response_array);

	    $client_add_campaigns = $this->_httpClientFactory->create();
	    $client_add_campaigns->setUri('https://api.pushassist.com/campaigns');
	    $client_add_campaigns->setHeaders('X-Auth-Token', $check_api_key);
            $client_add_campaigns->setHeaders('X-Auth-Secret', $check_secret_key);
            $client_add_campaigns->setHeaders('Content-Type', 'application/json');
            $client_add_campaigns->setMethod(\Zend_Http_Client::POST);
            $client_add_campaigns->setRawData($add_campaigns_request_json);
            $response_add_campaigns = $client_add_campaigns->request()->getBody();
	  } catch (\Exception $e) {
                $response_add_campaigns = "";
	  }
            return json_decode($response_add_campaigns, true);
     }

     public function gcm_setting($gcm_setting_response_array) {
	  try{
            $check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
	    $check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
            $gcm_request_json = json_encode($gcm_setting_response_array);

	    $gcm_setting = $this->_httpClientFactory->create();
	    $gcm_setting->setUri('https://api.pushassist.com/campaigns');
	    $gcm_setting->setHeaders('X-Auth-Token', $check_api_key);
            $gcm_setting->setHeaders('X-Auth-Secret', $check_secret_key);
            $gcm_setting->setHeaders('Content-Type', 'application/json');
            $gcm_setting->setMethod(\Zend_Http_Client::POST);
            $gcm_setting->setRawData($gcm_request_json);
            $response_gcm_setting = $gcm_setting->request()->getBody();
	  } catch (\Exception $e) {
                $response_gcm_setting = "";
	  }
            return json_decode($response_gcm_setting, true);
     }

     public function settings($settings_response_array){
	  try{
            $check_api_key = $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
	    $check_secret_key = $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
            $settings_request_json = json_encode($settings_response_array);

	    $settings = $this->_httpClientFactory->create();
	    $settings->setUri('https://api.pushassist.com/settings');
	    $settings->setHeaders('X-Auth-Token', $check_api_key);
            $settings->setHeaders('X-Auth-Secret', $check_secret_key);
            $settings->setHeaders('Content-Type', 'application/json');
            $settings->setMethod(\Zend_Http_Client::POST);
            $settings->setRawData($settings_request_json);
            $response_settings = $settings->request()->getBody();
	  } catch (\Exception $e) {
                $response_settings = "";
	  }
          return json_decode($response_settings, true);
      }
      
      public function getApiKey(){
	return $this->scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
      }
      public function getSecretKey(){
	return $this->scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
      }
      public function getJsRestrict(){
	return $this->scopeConfig->getValue('pushassistsection/general/pushassist_js_restrict',ScopeInterface::SCOPE_STORE);
      }
      public function getSettingPostMessage(){
	return $this->scopeConfig->getValue('pushassistsection/general/pushassist_setting_post_message',ScopeInterface::SCOPE_STORE);
      }
} 
