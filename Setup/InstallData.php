<?php
namespace Pushassist\Webpushnotification\Setup;
 
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
 
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }
 
   
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		
        /**
         * Add attributes to the eav/attribute
         */
	
	$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'push_custom_title');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,'push_custom_title',
            [				
	    'group' => 'PushAssist Notification',
            'type' => 'text',
            'backend' => '',
            'frontend' => '',
            'label'         => 'Notification Title',
            'input' => 'text',
	    'required' => false,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
	    'required' => false,
	    'user_defined' => true,
	    'default' => '',
	    'searchable' => false,
	    'filterable' => false,
	    'comparable' => false,
	    'visible_on_front' => false,
	    'used_in_product_listing' => true,
	    'unique' => false
            ]
        );
        
	$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'push_custom_message');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'push_custom_message',
            [				
	    'group' => 'PushAssist Notification',
	    'type' => 'text',
	    'backend' => '',
	    'frontend' => '',
	    'label'         => 'Notification Message',
	    'input' => 'text',				
	    'required' => false,
	     'note' 	    => 'Default product message.',
	   'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL, 
	    'visible' => true,
	    'required' => false,
	    'user_defined' => true,
	    'default' => '',
	    'searchable' => false,
	    'filterable' => false,
	    'comparable' => false,
	    'visible_on_front' => false,
	    'used_in_product_listing' => true,
	    'unique' => false
            ]
        );

	$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'push_custom_url');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'push_custom_url',
            [				
	    'group' => 'PushAssist Notification',
	    'type' => 'text',
	    'backend' => '',
	    'frontend' => '',
	    'label'         => 'Custom URL',
	    'input' => 'text',				
	    'required' => false,
	     'note' 	    => 'Product landing URL.',
	   'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL, 
	    'visible' => true,
	    'required' => false,
	    'user_defined' => true,
	    'default' => '',
	    'searchable' => false,
	    'filterable' => false,
	    'comparable' => false,
	    'visible_on_front' => false,
	    'used_in_product_listing' => true,
	    'unique' => false
            ]
        );
	$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'check_utm');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'check_utm',
            [				
	    'group' => 'PushAssist Notification',
	    'type' => 'int',
	    'backend' => '',
	    'frontend' => '',
	    'label' => 'UTM',
	    'input' => 'select',				
	    'required' => false,
	     'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
	    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL, 
	    'visible' => true,
	    'required' => false,
	    'user_defined' => true,
	    'default' => 'pushassist',
	    'searchable' => false,
	    'filterable' => false,
	    'comparable' => false,
	    'visible_on_front' => false,
	    'used_in_product_listing' => true,
	    'unique' => false
            ]
        );
	$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'push_utm_source');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'push_utm_source',
            [				
	    'group' => 'PushAssist Notification',
	    'type' => 'text',
	    'backend' => '',
	    'frontend' => '',
	    'label'         => 'UTM Source',
	    'input' => 'text',				
	    'required' => false,
	    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL, 
	    'visible' => true,
	    'required' => false,
	    'user_defined' => true,
	    'default' => 'pushassist',
	    'searchable' => false,
	    'filterable' => false,
	    'comparable' => false,
	    'visible_on_front' => false,
	    'used_in_product_listing' => true,
	    'unique' => false
            ]
        );

	$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'push_utm_medium');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'push_utm_medium',
            [				
	    'group' => 'PushAssist Notification',
	    'type' => 'text',
	    'backend' => '',
	    'frontend' => '',
	    'label'         => 'UTM Medium',
	    'input' => 'text',				
	    'required' => false,
	    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL, 
	    'visible' => true,
	    'required' => false,
	    'user_defined' => true,
	    'default' => 'pushassist_notification',
	    'searchable' => false,
	    'filterable' => false,
	    'comparable' => false,
	    'visible_on_front' => false,
	    'used_in_product_listing' => true,
	    'unique' => false
            ]
        );

	$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'push_utm_campaign');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'push_utm_campaign',
            [				
	    'group' => 'PushAssist Notification',
	    'type' => 'text',
	    'backend' => '',
	    'frontend' => '',
	    'label' => 'UTM Campaign',
	    'input' => 'text',				
	    'required' => false,
	    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL, 
	    'visible' => true,
	    'required' => false,
	    'user_defined' => true,
	    'default' => 'pushassist',
	    'searchable' => false,
	    'filterable' => false,
	    'comparable' => false,
	    'visible_on_front' => false,
	    'used_in_product_listing' => true,
	    'unique' => false
            ]
        );

	$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'enable_push_notification');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'enable_push_notification',
            [				
	    'group' => 'Product Details',
	    'type' => 'int',
	    'backend' => '',
	    'frontend' => '',
	    'label' => 'Send Notification',
	    'input' => 'boolean',				
	    'required' => false,
	    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL, 
	    'visible' => true,
	    'required' => false,
	    'user_defined' => true,
	    'default' => '0',
	    'searchable' => false,
	    'filterable' => false,
	    'comparable' => false,
	    'visible_on_front' => false,
	    'used_in_product_listing' => true,
	    'unique' => false
            ]
        );

      
    }
}

