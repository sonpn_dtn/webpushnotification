<?php 
namespace Pushassist\Webpushnotification\Observer\Adminhtml;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

class Product implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $logger;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
	\Magento\Store\Model\StoreManagerInterface $storeManager,
	\Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
	$this->_scopeConfig = $scopeConfig;
	$this->_storeManager = $storeManager;
	$this->logger = $loggerInterface;
        $this->_objectManager = $objectManager;
    }
 
    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
	  $product = $observer->getEvent()->getProduct();
	  $enable_notification=$product->getEnablePushNotification();

	  $apikey=$this->_scopeConfig->getValue('pushassistsection/general/apikey',ScopeInterface::SCOPE_STORE);
	  $secretkey=$this->_scopeConfig->getValue('pushassistsection/general/secretkey',ScopeInterface::SCOPE_STORE);
	if($apikey !='' && $secretkey !=''){
	  // Custom Title
	  $product_custom_title=$product->getPushCustomTitle();
	  if($product_custom_title != ''){
	      $product_notification_title=$product->getPushCustomTitle();
	  }else{
	      $product_notification_title=$product->getName();
	  }

	  // Custom Message
	  $product_custom_message=$product->getPushCustomMessage();
	  $prod_short_message=$product->getShortDescription();
	  $global_setting_message=$this->_scopeConfig->getValue('pushassistsection/general/pushassist_setting_post_message',ScopeInterface::SCOPE_STORE);

	  if($product_custom_message != ''){ 
	      $pushassist_setting_post_message=$product->getPushCustomMessage();
	  }elseif($prod_short_message != ''){
	      $pushassist_setting_post_message=$product->getShortDescription();
	  }elseif($global_setting_message != '' ){ 
	      $pushassist_setting_post_message=$this->_scopeConfig->getValue('pushassistsection/general/pushassist_setting_post_message',ScopeInterface::SCOPE_STORE);
	  }
	
	  //Custom URL
	  $product_custom_url=$product->getPushCustomUrl();
	  if($product_custom_url != ''){
	    $product_notification_url=$product->getPushCustomUrl();
	  }else{
	      $baseurl=$this->_storeManager->getStore()->getBaseUrl();
	      $suffix = $this->_scopeConfig->getValue('catalog/seo/product_url_suffix',ScopeInterface::SCOPE_STORE);
	      $product_notification_url = $baseurl.$product->getUrlKey().$suffix;
	  }

	  $baseImageUrl =$this->_storeManager->getStore()->getBaseUrl().'pub/media/catalog/product'.$product->getImage();
	  $smallImageUrl = $this->_storeManager->getStore()->getBaseUrl().'pub/media/catalog/product'.$product->getSmallImage();
	  $thumbnailUrl = $this->_storeManager->getStore()->getBaseUrl().'pub/media/catalog/product'.$product->getThumbnail();

	  $image_full_path='';

	  if($baseImageUrl !='' && $product->getImage() != 'no_selection'){
	    $image_full_path=$baseImageUrl;
	  }elseif($smallImageUrl !='' && $product->getSmallImage() != 'no_selection'){
	    $image_full_path=$smallImageUrl;
	  }elseif($thumbnailUrl !='' && $product->getThumbnail() != 'no_selection'){
	    $image_full_path=$thumbnailUrl;
	  }
	    
	  $response_array = array("notification" => array(
						  "title" => $product_notification_title,
						  "message" => $pushassist_setting_post_message,
						  "redirect_url" => $product_notification_url,
						  "image" => $image_full_path)
	  );

	  $product_utm=$product->getCheckUtm();

	  if(isset($product_utm)) {

		if($product_utm==1) {

		    $product_utm_source=$product->getCheckUtmSource();
		    if(isset($product_utm_source) && $product_utm_source !='') {
			    $product_utm_source=$product->getCheckUtmSource();
		    }else{
			    $product_utm_source='pushassist';
		    }

		    $product_utm_medium=$product->getCheckUtmMedium();
		    if(isset($product_utm_medium) && $product_utm_medium !='') {
			    $product_utm_medium=$product->getCheckUtmMedium();
		    }else{
			    $product_utm_medium='pushassist_notification';
		    }

		    $product_utm_campaign=$product->getCheckUtmCampaign();
		    if(isset($product_utm_campaign) && $product_utm_campaign !='') {
			    $product_utm_campaign=$product->getCheckUtmCampaign();
		    }else{
			    $product_utm_campaign='pushassist';
		    }
		    
		    $response_array['utm_params'] =array("utm_source" => $product_utm_source,	// optional
							"utm_medium" => $product_utm_medium,
							"utm_campaign" => $product_utm_campaign);
		}
 	}
	
	if($enable_notification==1 && $product->getStatus()==1 && $product->getVisibility()==4 ){ 
	      $result_array =$this->_objectManager->create('Pushassist\Webpushnotification\Helper\Data')->send_notification($response_array);
	}
	
      }  
        
    }
}